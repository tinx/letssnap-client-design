/**
 * Script to export the green key files
 *
 * @author Tim De Paepe <tim.depaepe@letssnap.be>
 */

// ----------
// PRE SCRIPT
// ----------

app.activeDocument.flipCanvas(Direction.HORIZONTAL);

// ------
// SCRIPT
// ------

// get the green key group
var greenKeyGroups = app.activeDocument.layerSets.getByName('GREEN-KEY');

// get the background group
var backgroundGroups = greenKeyGroups.layerSets.getByName('BG');

// get the overlay group
var overlayGroups = greenKeyGroups.layerSets.getByName('OVERLAY');

// show backgrounds and overlays
greenKeyGroups.visible = true;
backgroundGroups.visible = true;
overlayGroups.visible = true;

// hide groups
hideAllGroups(backgroundGroups);
hideAllGroups(overlayGroups);

// -------------
// SAVE OVERLAYS
// -------------

//  SHOW the TOP overlay group
var overlayGroupTop = overlayGroups.layerSets.getByName('STAY ON TOP');
if(overlayGroupTop.artLayers.length > 0) overlayGroupTop.visible = true;

// save the overlay layers
for (setIndex = 0; setIndex < overlayGroups.layerSets.length; setIndex++)
{
  // get the overlay group
  var overlayGroup = overlayGroups.layerSets[setIndex];

  // validate
  if(overlayGroup.name == 'STAY ON TOP') continue;

  // set visibility
  overlayGroup.visible = true;

  // save the overlay
  if(overlayGroup.artLayers.length > 0) { saveOverlay(setIndex); }

  // hide the layer
  overlayGroup.visible = false;
}

//  HIDE the TOP overlay group
if(overlayGroupTop.artLayers.length > 0) overlayGroupTop.visible = false;

// ----------------
// SAVE BACKGROUNDS
// ----------------

for (setIndex = 0; setIndex < backgroundGroups.layerSets.length; setIndex++)
{
  // get the groups
  var backgroundGroup = backgroundGroups.layerSets[setIndex];

  // set visibility
  backgroundGroup.visible = true;

  // save the background
  if(backgroundGroup.artLayers.length > 0) { saveBackground(setIndex + 1); }

  // hide them
  backgroundGroup.visible = false;
}

// -----------
// POST SCRIPT
// -----------

app.activeDocument.flipCanvas(Direction.HORIZONTAL);

// -----------------------------------------------------------------------------

// -----
// LOGIC
// -----

/**
 * Save A Background
 */

function saveBackground(number)
{
  var doc = app.activeDocument;
  var file = new File(doc.path + '/export/background_0' + number + '.jpg');

  saveForWeb(file, SaveDocumentType.JPEG, 75);
}

/**
 * Save An Overlay
 */

function saveForWeb(saveFile, format, quality)
{
  var sfwOptions = new ExportOptionsSaveForWeb();
     sfwOptions.format = format;
     // sfwOptions.includeProfile = false;
     sfwOptions.interlaced = 0;
     // sfwOptions.optimized = true;
     sfwOptions.quality = quality; //0-100
  app.activeDocument.exportDocument(saveFile, ExportType.SAVEFORWEB, sfwOptions);
}

function saveOverlay(number)
{
  var doc = app.activeDocument;
  var file = new File(doc.path + '/export/background_0' + number + '-overlay.png');

  saveForWeb(file, SaveDocumentType.PNG, 100);
}

/**
 * Hide all groups
 */

function hideAllGroups(parent)
{
  // hide all the visible layers
  for (setIndex = 0; setIndex < parent.layerSets.length; setIndex++)
  {
    // get the layersets
    var group = parent.layerSets[setIndex];

    // make them invisible
    group.visible = false;
  }
}
